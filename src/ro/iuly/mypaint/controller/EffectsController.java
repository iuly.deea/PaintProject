package ro.iuly.mypaint.controller;


import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.effect.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ro.iuly.mypaint.Main;
import ro.iuly.mypaint.form.DialogModalForm;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;


public class EffectsController {
    static FileChooser fileChooser = new FileChooser();

    public static void CreateEffects(ImageView ip, Stage window) {
        Button save = new Button("Save");

        ComboBox comboBox = new ComboBox();
        comboBox.getItems().setAll("Blur", "Shadow", "Lighten", "Bloom", "Motion Blur", "Reflection", "Normal");
        comboBox.setPromptText("Choose an effect");

        comboBox.setOnAction(e -> {
            if (comboBox.getValue() == "Blur") {
                BoxBlur boxBlur = new BoxBlur();
                ip.setEffect(boxBlur);
            }

            if (comboBox.getValue() == "Shadow") {
                DropShadow dropShadow = new DropShadow();
                ip.setEffect(dropShadow);
            }

            if (comboBox.getValue() == "Lighten") {
                Lighting light = new Lighting();
                ip.setEffect(light);
            }

            if (comboBox.getValue() == "Bloom") {
                Bloom bloomEffect = new Bloom();
                ip.setEffect(bloomEffect);
            }

            if (comboBox.getValue() == "Motion Blur") {
                MotionBlur motionBlur = new MotionBlur();
                ip.setEffect(motionBlur);
            }

            if (comboBox.getValue() == "Reflection") {
                Reflection reflection = new Reflection();
                ip.setEffect(reflection);
            }

            if (comboBox.getValue() == "Normal") {
                ip.setEffect(null);
            }

        });


        HBox photoButtons = new HBox(10);
        photoButtons.getChildren().addAll(comboBox, save);

        Main.layout_menus.setBottom(photoButtons);

        save.setOnAction(e -> saveImage(ip, window));


    }


    public static void saveImage(ImageView ip, Stage window) {

        BufferedImage bImage = SwingFXUtils.fromFXImage(ip.snapshot(null, null), null);
//              fromFXImage->snapshots the JavaFX Image object and stores a copy of its pixels into a BufferedImage obj
        File fileSave = fileChooser.showSaveDialog(window);

        try {
            ImageIO.write(bImage, "png", fileSave);
        } catch (Exception e) {
            new DialogModalForm(window, "Error", "Failed to save image");
        }
    }
}
