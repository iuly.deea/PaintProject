package ro.iuly.mypaint.controller;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import ro.iuly.mypaint.Main;
import ro.iuly.mypaint.State;
import ro.iuly.mypaint.form.ConfirmExitModalForm;

public class MenuController {

    static boolean menusInit = false;
    static MenuBar menuBar;
    static StackPane centerPane;

    public static void menus(Stage window) {
        if (!menusInit) {
            Menu fileMenu = new Menu("_File");
            MenuItem fileOpen = new MenuItem("Open");
            MenuItem fileExit = new MenuItem("Exit");

            fileExit.setOnAction(e -> {
                ConfirmExitModalForm.showConfirmExit(window);
            });

            fileOpen.setOnAction(e -> FileOpen.FileOpener(window));

            fileMenu.getItems().addAll(fileOpen, fileExit);


            //ADDING THE VIEW MENU
            Menu viewMenu = new Menu("_View");
            //ADDING ITEMS TO THE VIEW MENU
            MenuItem editFullScreen = new MenuItem("Enter/Exit Full Screen");
            viewMenu.getItems().addAll(editFullScreen);

            centerPane = new StackPane();


            editFullScreen.setOnAction(e -> {
                if (State.fullScreen == false) {
                    enableFullScreen(window);
                } else
                    disableFullScreen(window);
            });

            menuBar = new MenuBar();
            menuBar.getMenus().addAll(fileMenu, viewMenu);

            menusInit = true;

            if (FileOpen.ip != null) {
                centerPane.getChildren().addAll(FileOpen.ip);
            } else {
                centerPane.getChildren().addAll(Main.OpenCanvas);
            }
        }


        Main.layout_menus.setTop(menuBar);
        Main.layout_menus.setCenter(centerPane);
    }

    private static void enableFullScreen(Stage window) {
        window.setFullScreen(true);
        State.fullScreen = true;
    }

    private static void disableFullScreen(Stage window) {
        window.setFullScreen(false);
        State.fullScreen = false;
    }
}
