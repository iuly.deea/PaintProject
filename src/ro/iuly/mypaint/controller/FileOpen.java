package ro.iuly.mypaint.controller;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ro.iuly.mypaint.Main;
import ro.iuly.mypaint.State;
import ro.iuly.mypaint.form.DialogModalForm;

import java.io.File;


public class FileOpen {
    public static ImageView ip;

    static double orgSceneX, orgSceneY;
    static double orgTranslateX, orgTranslateY;

    static EventHandler<MouseEvent> imageOnMousePressedEventHandler =
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent t) {
                    orgSceneX = t.getSceneX();
                    orgSceneY = t.getSceneY();
                    orgTranslateX = ((ImageView) (t.getSource())).getTranslateX();
                    orgTranslateY = ((ImageView) (t.getSource())).getTranslateY();
                }
            };
    static EventHandler<MouseEvent> imageOnMouseDraggedEventHandler =
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent t) {
                    double offsetX = t.getSceneX() - orgSceneX;
                    double offsetY = t.getSceneY() - orgSceneY;
                    double newTranslateX = orgTranslateX + offsetX;
                    double newTranslateY = orgTranslateY + offsetY;
                    ((ImageView) (t.getSource())).setTranslateX(newTranslateX);
                    ((ImageView) (t.getSource())).setTranslateY(newTranslateY);
                }
            };

    static StackPane root = new StackPane();

    public static void FileOpener(Stage window) {
        FileChooser fileChooser = new FileChooser();
        File fileOpen = fileChooser.showOpenDialog(window);

        if (fileOpen != null) {
            Image image1 = new Image(fileOpen.toURI().toString());
            ip = new ImageView(image1);
            State.imageView = ip;

            ip.setPreserveRatio(true);
            ip.setSmooth(true);

            ip.fitWidthProperty().bind(Main.OpenCanvas.widthProperty());
            ip.fitHeightProperty().bind(Main.OpenCanvas.heightProperty());

            Slider sliderScaleImage = new Slider();

            sliderScaleImage.setMin(1);
            sliderScaleImage.setMax(50);
            sliderScaleImage.setShowTickLabels(true);
            sliderScaleImage.setShowTickMarks(true);
            sliderScaleImage.setOnMouseDragged(e -> {//zoom in the image
                ip.setScaleX(sliderScaleImage.getValue());
                ip.setScaleY(sliderScaleImage.getValue());
                ip.setScaleZ(sliderScaleImage.getValue());
            });

            root.setAlignment(sliderScaleImage, Pos.TOP_LEFT);

            if (root.getChildren().size() == 0) {
                root.getChildren().addAll(Main.OpenCanvas, ip, sliderScaleImage);

            } else {
                root.getChildren().set(1, ip);
            }

            Main.layout_menus.setLeft(root);

            ip.setCursor(Cursor.HAND);
            ip.setOnMousePressed(imageOnMousePressedEventHandler);
            ip.setOnMouseDragged(imageOnMouseDraggedEventHandler);

        } else {
            DialogModalForm.showAlert(window, "Alert", "You have cancelled opening an image!!");
        }
    }

}
