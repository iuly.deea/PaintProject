package ro.iuly.mypaint.controller;


import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.FillRule;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ro.iuly.mypaint.Main;
import ro.iuly.mypaint.form.DialogModalForm;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.File;


public class TestBrush {

    public static Canvas canvas = new Canvas(800, 500);
    static boolean brPanInit = false;
    static GridPane grid = new GridPane();
    static GraphicsContext gc;
    static StackPane rootPane = new StackPane();
    static StackPane paneSave = new StackPane();
    static Slider slider;
    static CheckBox eraser;

    static EventHandler<MouseEvent> onMouseDraggedHandler = e -> {
        if (eraser.isSelected()) {
            gc.clearRect(e.getX(), e.getY(), slider.getValue(), slider.getValue());
        } else {
            gc.lineTo(e.getSceneX(), e.getSceneY());


            gc.stroke();
        }
    };

    static EventHandler<MouseEvent> onMousePressedHandler = e -> {
        if (eraser.isSelected()) {
            gc.clearRect(e.getX(), e.getY(), slider.getValue(), slider.getValue());
        } else {
            gc.beginPath();
            gc.lineTo(e.getSceneX(), e.getSceneY());
            gc.stroke();
        }
    };


    public static void testBrush(Scene scene, Stage window) {
        if (!brPanInit) {
            ColorPicker colorPicker = new ColorPicker();
            eraser = new CheckBox("Eraser");
            slider = new Slider();

            Button saveDrawing = new Button("Save");

            canvas.widthProperty().bind(Main.layout_menus.widthProperty());
            canvas.heightProperty().bind(Main.layout_menus.heightProperty());

            gc = canvas.getGraphicsContext2D();

            gc.setStroke(Color.BLACK);//color by default
            gc.setLineWidth(1);//line thickness by default

            gc.setFillRule(FillRule.EVEN_ODD);

            gc.setLineCap(StrokeLineCap.ROUND);



/*
            colorPicker.setValue(Color.BLACK);

            CheckBox backgroundCheckBox = new CheckBox("Background");
            colorPicker.setOnAction(e -> {
                gc.setStroke(colorPicker.getValue());


                if (backgroundCheckBox.isSelected()) {
                    Paint fill = colorPicker.getValue();
                    BackgroundFill backgroundFill =
                            new BackgroundFill(fill,
                                    CornerRadii.EMPTY,
                                    Insets.EMPTY);
                    Background background = new Background(backgroundFill);
                    paneSave.setBackground(background);
                }
            });
*/

            slider.setMin(1);
            slider.setMax(100);
            slider.setShowTickLabels(true);
            slider.setShowTickMarks(true);
            slider.setOnMouseDragged(e -> gc.setLineWidth(slider.getValue()));

            saveDrawing.setOnAction(e -> saveDrawing(window));

            Button clearCanvas = new Button("Clear Canvas");
            clearCanvas.setOnAction(e -> gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight()));


            Button returnButton = new Button("Return to main window");


            if (FileOpen.ip != null) {
                paneSave.getChildren().addAll(FileOpen.ip, canvas);
                rootPane.getChildren().addAll(paneSave, grid);
            } else {
                paneSave.getChildren().addAll(Main.OpenCanvas, canvas);
                rootPane.getChildren().addAll(paneSave, grid);
            }

            returnButton.setOnAction(e -> {
                rootPane.setVisible(false);
                disableBrush(scene);
                Main.layout_menus.setTop(null);
                MenuController.menus(window);//sets the menus
            });

            grid.addRow(0, colorPicker, slider, eraser, saveDrawing, clearCanvas);
//            grid.addRow(1, backgroundCheckBox, returnButton);

            grid.setAlignment(Pos.TOP_CENTER);
            grid.setPadding(new Insets(0, 0, 0, 0));

            brPanInit = true;
        }

        Main.layout_menus.setCenter(null);
        Main.layout_menus.setLeft(null);
        Main.layout_menus.setRight(null);
        Main.layout_menus.setTop(rootPane);
        enableBrush(scene);
        rootPane.setVisible(true);
    }

    public static void saveDrawing(Stage window) {
        FileChooser fileChooser = new FileChooser();

        BufferedImage bImage = SwingFXUtils.fromFXImage(paneSave.snapshot(null, null), null);

        File fileSave = fileChooser.showSaveDialog(window);
        try {
            ImageIO.write(bImage, "png", fileSave);//fileSave=output file

        } catch (Exception e) {
            new DialogModalForm(window, "Error", "Failed to save image");
        }
    }

    public static void enableBrush(Scene scene) {
        scene.setOnMousePressed(onMousePressedHandler);
        scene.setOnMouseDragged(onMouseDraggedHandler);
    }

    public static void disableBrush(Scene scene) {
        scene.setOnMousePressed(null);
        scene.setOnMouseDragged(null);
    }
}
