package ro.iuly.mypaint;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import ro.iuly.mypaint.controller.EffectsController;
import ro.iuly.mypaint.controller.FileOpen;
import ro.iuly.mypaint.controller.MenuController;
import ro.iuly.mypaint.controller.TestBrush;
import ro.iuly.mypaint.form.DialogModalForm;

import java.net.URL;


public class Main extends Application {
    public static BorderPane layout_menus = new BorderPane();
    public static Canvas OpenCanvas = new Canvas(650, 400);
    static Stage window;
    static Scene scene;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Paint Project");
        MenuController.menus(window);
        layoutButtons();

        Image projectIcon = new Image("https://scottiestech.info/wp-content/uploads/2009/08/paint.jpg");
        window.getIcons().add(projectIcon);


        scene = new Scene(layout_menus, 900, 600);
        window.setScene(scene);
        window.show();
    }


    public void layoutButtons() {
        Image brushImage = new Image(loadIcon("brush.jpg"), 70, 70, true, true);
        Button brushButton = new Button();
        brushButton.setGraphic(new ImageView(brushImage));

        brushButton.setOnAction(e -> TestBrush.testBrush(scene, window));


        Image filtersImage = new Image(loadIcon("filters.png"), 70, 70, true, true);
        Button filtersButton = new Button();
        filtersButton.setGraphic(new ImageView(filtersImage));

        filtersButton.setOnAction(e -> {
            if (State.imageView == null) {
                DialogModalForm.showAlert(window, "Warning", "Please open an image before applying filters");
            } else {
                EffectsController.CreateEffects(FileOpen.ip, window);
            }
        });


        HBox layoutButtons = new HBox();
        layoutButtons.getChildren().addAll(brushButton, filtersButton);
        layout_menus.setBottom(layoutButtons);
    }

    private String loadIcon(String iconImage) {
        URL resource = Main.class.getClassLoader().getResource(iconImage);
        return resource.toExternalForm();
    }
}
