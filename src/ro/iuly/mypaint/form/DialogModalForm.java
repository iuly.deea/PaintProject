package ro.iuly.mypaint.form;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class DialogModalForm implements ModalForm {
    Stage parent;
    String title;
    String message;
    Stage window;

    public DialogModalForm(Stage parent, String title, String message) {
        this.parent = parent;
        this.title = title;
        this.message = message;
        initialize();
    }

    @Override
    public void initialize() {

        window = new Stage();
        Button okButton = new Button("Ok");

        okButton.setOnAction(e -> {
            window.close();
        });

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);

        window.setWidth(500);
        Label label = new Label(message);
        VBox layout = new VBox(20);
        layout.getChildren().addAll(label, okButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout, 500, 200);
        window.setScene(scene);
    }

    public boolean show() {
        window.showAndWait();
        return true;
    }


    public static void showAlert(Stage parent, String title, String message) {
        new DialogModalForm(parent, title, message).show();
    }

}
