package ro.iuly.mypaint.form;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class ConfirmExitModalForm implements ModalForm {
    boolean answer;
    Stage parent;
    String title;
    String message;
    Stage window;

    private ConfirmExitModalForm(Stage parent, String title, String message) {
        this.parent = parent;
        this.title = title;
        this.message = message;
        initialize();
    }

    public static boolean showConfirmExit(Stage parent) {
        return new ConfirmExitModalForm(parent, "Confirm Exit", "Are you sure you want to exit?").show();
    }

    @Override
    public void initialize() {
        window = new Stage();
        Button YesButton = new Button("YES");
        Button NoButton = new Button("NO");

        YesButton.setOnAction(e -> {
            answer = true;
            window.close();
            parent.close();
        });

        NoButton.setOnAction(e -> {
            answer = false;
            window.close();
        });

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setWidth(500);
        Label label = new Label(message);
        VBox layout = new VBox(20);
        layout.getChildren().addAll(label, YesButton, NoButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout, 500, 200);
        window.setScene(scene);
    }

    public boolean show() {
        window.showAndWait();
        return answer;
    }

}
