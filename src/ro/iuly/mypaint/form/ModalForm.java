package ro.iuly.mypaint.form;

public interface ModalForm {

    public void initialize();

    public boolean show();


}
